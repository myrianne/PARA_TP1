# README

Ce logiciel vise à recenser les données aux registres de la Ville de Montréal pour vous offrir des statistiques sous diverses formes concernant ses lieux culturels ainsi que les crimes ayant eu lieu dans la métropole.


## *Comment l'installer?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal puis tapez:

    > npm install

## *Comment l'exécuter?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal puis tapez:

    > npm start

## *Où trouver la documentation?*

La documentation des services services REST est accessible via :

* le localhost port 3000 [http://localhost:3000/doc](http://localhost:3000/doc) 
* la  plateforme infonuagique Heroku [https://fathomless-dawn-43279.herokuapp.com/doc](https://fathomless-dawn-43279.herokuapp.com/doc). 


## Auteure

* Myrianne Beaudoin-Thériault *BEAM26588114*








