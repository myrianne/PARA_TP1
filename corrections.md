# Corrections

**Il est possible de visionner facilement les diverses routes disponibles via la page d'accueil du site.** 

Allez directement au logiciel déployé sur Heroku :


* [https://fathomless-dawn-43279.herokuapp.com/](https://fathomless-dawn-43279.herokuapp.com/)

Ou déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal et tapez :

    > npm start

Puis sur un navigateur web, entrez l'adresse suivante :

* [http://localhost:3000/](http://localhost:3000/)



## *A1*
*Le script* `Node.js` *, situé à la racine du dossier, permet d’extraire les données de la ville de Montréal sur les actes criminels et de construire une base de données MongoDB ' Crimes ', collection ' crimes '.*

#####*comment tester?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal puis tapez :

    > node Node.js



## *A2*
*A2, situé dans le fichier* `routes/index.js` *extrait le code d’importation de données du point A1* `Node.js` *et lance un ' cronJob ' afin d’importer automatiquement les données sur les actes criminels municipaux chaque jour à 13h30.*

#####*comment tester?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal et tapez :

    > npm start

Pour tester, il est possible de modifier l'heure du ' cronJob ' dans le fichier `routes/index.js` *ligne 11*.



## *A3*
*La route* `/doc`*, située dans le fichier* `routes/index.js` *affiche la documentation de tous les services web. La documentation est en format HTML, générée à partir du fichier RAML dans* `doc/doc.raml`

#####*comment tester?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal et tapez :

    > npm start

Puis sur un navigateur web, entrez l'une des deux adresses suivantes:

* [http://localhost:3000/doc](http://localhost:3000/doc)


Ou allez directement au site déployé sur Heroku :


* [https://fathomless-dawn-43279.herokuapp.com/doc](https://fathomless-dawn-43279.herokuapp.com/doc)



## *A4*
*La route* `/actes`, *située dans le fichier* `routes/index.js`*, offre un service REST permettant d'obtenir la liste des actes criminels pour une date spécifiée en paramètre. 
Les données retournées sont en format JSON. La date en paramètre est en format ISO 8601. Si la date n'est pas donnée en paramètre, l'ensemble des données est retourné.*

#####*comment tester?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal et tapez:

    > npm start

Puis sur un navigateur web entrez, `http://localhost:3000/actes?date=` suivi d'une date entre `2015-01-01` et `2018-04-01` ou l'une des deux adresses suivantes :

* [http://localhost:3000/actes](http://localhost:3000/actes)


Ou allez directement au site déployé sur Heroku :


* [https://fathomless-dawn-43279.herokuapp.com/actes](https://fathomless-dawn-43279.herokuapp.com/actes)



## *A5*
*La route* `/stats/json`, *située dans le fichier* `routes/index.js`*, offre un service REST permettant d’obtenir des statistiques mensuelles sur les actes criminels survenus, en format JSON.*

#####*comment tester?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal et tapez :

    > npm start

Puis, sur un navigateur web entrez l'une des deux adresses suivantes :

* [http://localhost:3000/stats/json](http://localhost:3000/stats/json)


Ou allez directement au site déployé sur Heroku :


* [https://fathomless-dawn-43279.herokuapp.com/stats/json](https://fathomless-dawn-43279.herokuapp.com/stats/json)



## *A6*
*La route* `/stats/xml`, *située dans le fichier* `routes/index.js`*, offre un service REST permettant d’obtenir des statistiques mensuelles sur les actes criminels survenus, en format XML.*

#####*comment tester?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal et tapez :

    > npm start

Puis, sur un navigateur web entrez l'adresse suivante :

* [http://localhost:3000/stats/xml](http://localhost:3000/stats/xml)

Ou allez directement au site déployé sur Heroku :

* [https://fathomless-dawn-43279.herokuapp.com/stats/xml](https://fathomless-dawn-43279.herokuapp.com/stats/xml)



## *A7*
*La route* `/stats/buffer`, *située dans le fichier* `routes/index.js`*, offre un service REST permettant d’obtenir des statistiques mensuelles sur les actes criminels survenus, en format ' ProcolBuffer '.*

#####*comment tester?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal et tapez :

    > npm start

Puis, sur un navigateur web entrez l'adresse suivante :

* [http://localhost:3000/stats/buffer](http://localhost:3000/stats/buffer)

Ou allez directement au site déployé sur Heroku :

* [https://fathomless-dawn-43279.herokuapp.com/stats/buffer](https://fathomless-dawn-43279.herokuapp.com/stats/buffer)



## *B1*
*La route* `/stats/html` *, située dans le fichier* `routes/index.js`*, présente les données de A5 dans un document HTML. Au chargement de la page HTML, une requête Ajax invoque le service et affiche les statistiques reçues dans un tableau.*

#####*comment tester?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal et tapez :

    > npm start

Puis, sur un navigateur web entrez l'adresse suivante :

* [http://localhost:3000/stats/html](http://localhost:3000/stats/html)

Ou allez directement au site déployé sur Heroku :

* [https://fathomless-dawn-43279.herokuapp.com/stats/html](https://fathomless-dawn-43279.herokuapp.com/stats/html)



## *B2*
*La route* `/stats/html_chart`*, située dans le fichier* `routes/index.js`*, ajoute un bar chart à la page développée pour B1, illustrant l’évolution des différents types d’actes criminels concernant les 4 derniers mois aux 
registres.*

#####*comment tester?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal et tapez :

    > npm start

Puis, sur un navigateur web entrez l'adresse suivante :

* [http://localhost:3000/stats/htmlChart](http://localhost:3000/stats/html_chart)

Ou allez directement au site déployé sur Heroku :

* [https://fathomless-dawn-43279.herokuapp.com/stats/htmlChart](https://fathomless-dawn-43279.herokuapp.com/stats/html_chart)



## *C1*
*La route* `/actes3km`*, située dans le fichier* `routes/index.js`*, retourne la liste des actes criminels ayant eu lieu dans les 3 derniers mois situés à moins de 3 km d’un point donné en paramètre. La distance est calculée selon la formule de Haversine, format JSON.*

#####*comment tester?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal et tapez :

    > npm start

Puis sur un navigateur web entrez, `http://localhost:3000/actes3km?` suivi de
`latitude=` la latitude recherchée, `&longitude=` la longitude recherchée.

* [http://localhost:3000/actes3km?
latitude=45.5372886&longitude=-73.5904142](http://localhost:3000/actes3km?latitude=45.5372886&longitude=-73.5904142)

Ou allez directement au site déployé sur Heroku :

* [https://fathomless-dawn-43279.herokuapp.com/actes3km?
latitude=45.5372886&longitude=-73.5904142](https://fathomless-dawn-43279.herokuapp.com/actes3km?latitude=45.5372886&longitude=-73.5904142)



## *D1*
*Le script* `D1.js` *, situé à la racine du dossier, permet d’extraire les données de la ville de Montréal sur les lieux culturels et de construire une base de données MongoDB ' Crimes ', collection ' lieux '. Puis lance un ' cronJob ', située dans le fichier* `routes/index.js`*,  afin d’importer automatiquement les données sur les lieux culturels municipaux chaque jour à 13h31.*

#####*comment tester?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal puis tapez :

    > node D1.js

Pour tester, il est possible de modifier l'heure du ' cronJob ' dans le fichier `routes/index.js` *ligne 362*.

## *D2*
*La route `/lieux` retourne la liste des lieux culturels à Montréal. Pour chaque lieu, on retourne également le nombre d’actes criminels 
ayant eu lieu à moins de 1 km du lieu en 2018. La  distance est calculée selon la formule de Haversine*

#####*comment tester?*

Déplacez-vous à la racine du dossier `INF4375_BEAM26588114/` dans un terminal et tapez :

    > npm start

Puis, sur un navigateur web entrez l'adresse suivante :

* [http://localhost:3000/lieux](http://localhost:3000/lieux)

Ou allez directement au site déployé sur Heroku :

* [https://fathomless-dawn-43279.herokuapp.com/lieux](https://fathomless-dawn-43279.herokuapp.com/lieux)



## *F1*
*Le système est entièrement déployé sur la plateforme infonuagique Heroku :*

* [https://fathomless-dawn-43279.herokuapp.com/](https://fathomless-dawn-43279.herokuapp.com/)











