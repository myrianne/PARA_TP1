//--------------------------------------------------------------------------------D1.JS
//-------------------------------------------------------------------------------------
//	-------> D1
//	extrait les données de la ville de Montréal sur les lieux culturels
//  municipaux sur le territoire de la ville et l’insérer dans la base de données MongoDB.

const get = require('simple-get')
var http = require('http');
var fs = require('fs');
var csv = require('csv');
var obj = csv();
var obj2 = csv();
var iconv = require("iconv-lite");
var haversine = require('haversine');

module.exports = {
  createDB2: function() {
//--------------------------------------------------------------get csv 
    var file = fs.createWriteStream("lieuxculturels.csv");
    var request = http.get("http://donnees.ville.montreal.qc.ca/dataset/ceb2427e-aa50-"
    		+"4d06-b13a-d1b21e2702b9/resource/974ca8d4-ed85-4c77-a79f-9f765e4c1b32/"
    		+"download/lieuxculturels.csv", function(response) {
      response.pipe(file);
    });
    var input = fs.readFileSync("lieuxculturels.csv", {
      encoding: "binary"
    });
    var output = iconv.decode(input, "ISO-8859-1");
//--------------------------------------------------------------connect to database
    file.on('pipe', function() {
      console.log(" \n...data downloading lieuxculturels.csv...\n");
    });
    request.on('close', function() {
      var MongoClient = require("mongodb").MongoClient;
      var url = "mongodb://localhost:27017";
      MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("Crimes");
        var listeLieux = [];
        var listCrimes = [];
//--------------------------------------------------------------create json data
        obj.from.path('lieuxculturels.csv').to.array(function(data) {
          obj2.from.path('interventionscitoyendo.csv').to.array(function(data2) {
            for (var index = 1; index < data.length; index++) {
              var nb = 0;
              for (var index2 = 1; index2 < data2.length; index2++) {
                if (data2[index2][1].substring(0, 4) == '2018') {
                  var start = {
                    latitude: data[index][9],
                    longitude: data[index][10]
                  };
                  var end = {
                    latitude: data2[index2][7],
                    longitude: data2[index2][6]
                  };
                  if (haversine(start, end, {
                      unit: 'km'
                    }) <= 1) {
                    nb++;
                  }
                }
              }
              var lieu = {
                ARRONDISSEMENT: data[index][0],
                NOM_RESEAU: data[index][1],
                NOM_LIEU: data[index][2],
                ADRESSE: data[index][3],
                CODE_POSTAL: data[index][4],
                VILLE: data[index][5],
                PROVINCE: data[index][6],
                TELEPHONE: data[index][7],
                SITE_WEB: data[index][8],
                LATITUTE: parseFloat(data[index][9]),
                LONGITUTE: parseFloat(data[index][10]),
                DESCRIPTION: data[index][11],
                NB_CRIMES: nb
              };
              console.log(lieu);
              listeLieux.push(lieu);
            }
//--------------------------------------------------------------drop db if exists  
            dbo.collection("lieux").countDocuments()
              .then(function(num) {
                if (num > 0) {
                  dbo.collection("lieux").drop();
                  console.log(num);
                }
              });
//--------------------------------------------------------------create db
            dbo.collection("lieux").insertMany(listeLieux, function(err, res) {
              if (err) {
                throw err;
              } else {
                console.log("\n...download of lieuxculturels.csv done!\n");
              }
            });
          });
        });
      });
    });
  }
};
//--------------------------------------------------------------run function(private)
var another = require('./D1.js');
another.createDB2();