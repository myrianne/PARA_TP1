function replaceContent() {
				var body = document.getElementById("body");
				body.innerHTML = "<br><p> ... téléchargement en cours ...</p>";
				var lesMois = [];
				var mois = [],mois2 = [], mois3 = [], mois4 = [];
				var request = new XMLHttpRequest();
				var url = "http://localhost:3000/stats/json";
				request.open("GET", url, true);

				request.onreadystatechange = function() {
					
					var json = request.responseText;
					if (request.readyState === 4 && request.status === 200) {
						var tbody = document.getElementById("body");
						var stats = JSON.parse(json);
						tbody.innerHTML = "";
						for (var i=0; i < stats.length; i++) {		
							
							var row = tbody.insertRow(0);
							var cell0 = row.insertCell(0);
							var cell1 = row.insertCell(1);
							var cell2 = row.insertCell(2);
							var cell3 = row.insertCell(3);
							var cell4 = row.insertCell(4);
							var cell5 = row.insertCell(5);
							var cell6 = row.insertCell(6);
							var cell7 = row.insertCell(7);
							var cell8 = row.insertCell(8);
							var cell9 = row.insertCell(9);
							var cell10 = row.insertCell(10);
							
							cell0.innerHTML =  "&emsp;" + stats[i].mois;
							
							for(var j=0; j < 5; j++) {
								
								if (stats[i].actes[j].type == 'Introduction') {
									cell3.innerHTML =  "&emsp;" + stats[i].actes[j].type;
									cell4.innerHTML =  "&emsp;" + stats[i].actes[j].nombre;
								}
								if (stats[i].actes[j].type == 'Méfait') {
									cell1.innerHTML =  "&emsp;" + stats[i].actes[j].type;
									cell2.innerHTML =  "&emsp;" + stats[i].actes[j].nombre;
								}
								if (stats[i].actes[j].type == 'Vols qualifiés') {
									cell5.innerHTML =  "&emsp;" + stats[i].actes[j].type;
									cell6.innerHTML =  "&emsp;" + stats[i].actes[j].nombre;
								}
								if (stats[i].actes[j].type == 'Vol de véhicule à moteur') {
									cell7.innerHTML =  "&emsp;" + stats[i].actes[j].type;
									cell8.innerHTML =  "&emsp;" + stats[i].actes[j].nombre;
								}
								if (stats[i].actes[j].type == 'Vol dans / sur véhicule à moteur') {
									cell9.innerHTML =  "&emsp;" + stats[i].actes[j].type;
									cell10.innerHTML =  "&emsp;" + stats[i].actes[j].nombre;
								}
							}	
						}	
						
					}
					
					var json = request.responseText;
					var stats = JSON.parse(json);
					if (request.readyState === 4 && request.status === 200) {
						var mois_l = stats.length - 4;
						mois.push(['Vols qualifiés', stats[mois_l].actes[0].nombre],
									['Vol de véhicule à moteur', stats[mois_l].actes[1].nombre],
									['Méfait', stats[mois_l].actes[2].nombre],
									['Vol dans / sur véhicule à moteur', stats[mois_l].actes[3].nombre],
									['Introduction', stats[mois_l].actes[4].nombre]);
						mois2.push(['Vols qualifiés', stats[mois_l+1].actes[0].nombre],
									['Vol de véhicule à moteur', stats[mois_l+1].actes[1].nombre],
									['Méfait', stats[mois_l+1].actes[2].nombre],
									['Vol dans / sur véhicule à moteur', stats[mois_l+1].actes[3].nombre],
									['Introduction', stats[mois_l+1].actes[4].nombre]);
						mois3.push(['Vols qualifiés', stats[mois_l+2].actes[0].nombre],
									['Vol de véhicule à moteur', stats[mois_l+2].actes[1].nombre],
									['Méfait', stats[mois_l+2].actes[2].nombre],
									['Vol dans / sur véhicule à moteur', stats[mois_l+2].actes[3].nombre],
									['Introduction', stats[mois_l+2].actes[4].nombre]);
						mois4.push(['Vols qualifiés', stats[mois_l+3].actes[0].nombre],
									['Vol de véhicule à moteur', stats[mois_l+3].actes[1].nombre],
									['Méfait', stats[mois_l+3].actes[2].nombre],
									['Vol dans / sur véhicule à moteur', stats[mois_l+3].actes[3].nombre],
									['Introduction', stats[mois_l+3].actes[4].nombre]);	
						
						 
						anychart.onDocumentReady(function() {
			        		var chart = anychart.bar(mois);
			        		chart.tooltip()
			        			.titleFormat('{%X}')
			        			.format('{%Value}');
			        			
			        		chart.barGroupsPadding(0);
							chart.title(stats[mois_l].mois);
							chart.container("container");
							chart.yScale().maximum(600);
							chart.draw();
							
							var chart2 = anychart.bar(mois2);
							chart2.tooltip()
			        			.titleFormat('{%X}')
			        			.format('{%Value}');
			        		chart2.barGroupsPadding(0);
							chart2.title(stats[mois_l+1].mois);
							chart2.container("container2");
							chart2.yScale().maximum(600);
							chart2.draw();
							
							var chart3 = anychart.bar(mois3);
							chart3.tooltip()
			        			.titleFormat('{%X}')
			        			.format('{%Value}');
			        		chart3.barGroupsPadding(0);
							chart3.title(stats[mois_l+2].mois);
							chart3.container("container3");
							chart3.yScale().maximum(600);
							chart3.draw();
							
							var chart4 = anychart.bar(mois4);
							chart4.tooltip()
			        			.titleFormat('{%X}')
			        			.format('{%Value}');
			        		chart4.barGroupsPadding(0);
							chart4.title(stats[mois_l+3].mois);
							chart4.container("container4");
							chart4.yScale().maximum(600);
							chart4.draw();
						});
						
					}
				}
				
				request.send()
			}
			
