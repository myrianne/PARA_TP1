//--------------------------------------------------------------------------------A1.JS
//-------------------------------------------------------------------------------------
//	-------> A2
//	A2 extrait le code d’importation de données du point A1(A1.js) 
//	et lance un cron job afin d’importer automatiquement les données 
//	sur les actes criminels municipaux chaque jour à 13h30.
var express = require('express');
var router = express.Router();
var CronJob = require('cron').CronJob;
console.log("\n...refresh of data set for 13h30...\n");
cronJob = new CronJob('00 30 13 * * *', function() {
    var another = require('Node.js');
  }, function(err, data) {
    if(err) {
      res.status(500);
      res.render('error');
      console.log("\n--> erreur lors du CronJob(interventionscitoyendo.csv)\n");
    }
  },
  true
);

//-------------------------------------------------------------------------ROUTE '/DOC'
//-------------------------------------------------------------------------------------
//	-------> A3
//	La route «/doc» affiche la documentation de tous les services 
//	web (JSON ou XML). La documentation est en format HTML, générée 
//	à partir du fichiers RAML dans 'doc/doc.raml'. Un json-schema est
//  disponible pour les services envoyant du JSON.

router.get('/doc', function(req, res, next) {
  var raml2html = require('raml2html');
  var config = raml2html.getDefaultConfig(false);
  var onError = function(err) {
    res.status(500);
    res.render('error');
    console.log("\n--> erreur : " + err + "\n");
  };
  var onSuccess = function(html) { res.send(html); };
  raml2html.render("doc/doc.raml", config).then(onSuccess, onError);
});

//-----------------------------------------------------------------------ROUTE '/ACTES'
//-------------------------------------------------------------------------------------
//	-------> A4
//	La route «/actes» offre un service REST permettant d'obtenir la 
//  liste des actes criminels pour une date spécifiée en paramètre. 
//	Les données retournées sont en format JSON. La date en paramètre 
//  est en format ISO 8601. Si la date n'est pas donnée en paramètre,
//  l'ensemble des données est retourné.

router.get('/actes', function(req, res, next) {
  var date = req.query.date;
  var MongoClient = require("mongodb").MongoClient;
  var url = "mongodb://localhost:27017";
//--------------------------------------------------------------connect to DB  
  MongoClient.connect(url, function(err, db) {
    if(err) throw err;
    var dbo = db.db("Crimes");
    var crimes = [];
    if(req.query.date == undefined) {
      crimes = dbo.collection("crimes").find();
    } else {
      crimes = dbo.collection("crimes").find({ DATE: date });
    }
    if(err) {
      res.sendStatus(500);
    } else {
      crimes.toArray(function(err, liste) {
        if(liste.length == 0) {
          res.status(404);
          res.render('error404');
        } else {
          console.log(liste);
          res.json(liste);
        }
      });
    }
  });
});

//-------------------------------------------------------------------ROUTE '/STATS/JSON'
//-------------------------------------------------------------------------------------
//	-------> A5
//	La route «/stats/json» offre un service REST permettant d’obtenir 
//  des statistiques mensuelles sur les actes criminels survenus 
//  en format JSON.

router.get('/stats/json', function(req, res, next) {
  var MongoClient = require("mongodb").MongoClient;
  var url = "mongodb://localhost:27017";
  var month, lastMonth;
  //--------------------------------------------------------------connect to DB  
  MongoClient.connect(url, function(err, db) {
    if(err) throw err;
    var dbo = db.db("Crimes");
    var stats = [];
    //--------------------------------------------------------------get liste de mois  
    dbo.collection("crimes").distinct("DATE")
      .then(function(dates) {
        var listeCategories = ['Introduction', 'Méfait', 'Vols qualifiés',
          'Vol de véhicule à moteur', 'Vol dans / sur véhicule à moteur'
        ];
        dates.forEach(function(data) {
          month = data.substring(0, 8);
          if(month != lastMonth && month != 'DATE') {
            var leMois = data.substring(0, 7);
            var stat = [];
            //---------------------------------------------------get stats/categorie 			  
            listeCategories.forEach(function(categorie) {
              dbo.collection("crimes").find({
                  CATEGORIE: categorie,
                  DATE: new RegExp(month)
                }).count()
                .then(function(num) {stat.push({ type: categorie, nombre: num });});
            });
            stats.push({ mois: leMois, actes: stat });
          }
          lastMonth = month;
        });
        //--------------------------------------------------------------return json API
        setTimeout(function() {
          res.json(stats);
        }, 3000);
      });
  });
});

//-------------------------------------------------------------------ROUTE '/STATS/XML'
//-------------------------------------------------------------------------------------
//	-------> A6
//	La route «/stats/json» offre un service REST permettant d’obtenir 
//  des statistiques mensuelles sur les actes criminels survenus 
//  en format XML.

router.get('/stats/xml', function(req, res, next) {
  var MongoClient = require("mongodb").MongoClient;
  var url = "mongodb://localhost:27017";
  var lastMonth, month;
  var js2xmlparser = require("js2xmlparser");
  var beautify = require('xml-beautifier');
  MongoClient.connect(url, function(err, db) {
    var dbo = db.db("Crimes");
    var stats = [];
    //--------------------------------------------------------------get liste de mois  
    dbo.collection("crimes").distinct("DATE")
      .then(function(dates) {
        var listeCategories = ['Introduction', 'Méfait', 'Vols qualifiés',
          'Vol de véhicule à moteur', 'Vol dans / sur véhicule à moteur'
        ];
        dates.forEach(function(data) {
          month = data.substring(0, 8);
          if(month != lastMonth && month != 'DATE') {
            var leMois = data.substring(0, 7);
            var stat = [];
            //------------------------------------------------------get stats/categorie 			  
            listeCategories.forEach(function(categorie) {
              dbo.collection("crimes").find({
                  CATEGORIE: categorie,
                  DATE: new RegExp(month)
                }).count()
                .then(function(num) {
                  stat.push({ type: categorie, nombre: num });
                });
            });
            stats.push({ mois: leMois, actes: stat });
          }
          lastMonth = month;
        });
        //----------------------------------------------------------return xml API
        setTimeout(function(err) {
          if(err) {
            console.log("\n--> erreur lors du chargement de la page '/stats/xml'\n");
            res.status(500);
            res.render('error');
          }
          res.send(js2xmlparser.parse("crimes", stats));
        }, 3000);
      });
  });
});

//-----------------------------------------------------------------ROUTE '/STATS/BUFFER'
//-------------------------------------------------------------------------------------
//	-------> A7
//	La route «/stats/buffer» offre un service REST permettant d’obtenir 
//  des statistiques mensuelles sur les actes criminels survenus 
//  en format ProtocolBuffer.

router.get('/stats/buffer', function(req, res, next) {
  var MongoClient = require("mongodb").MongoClient;
  var url = "mongodb://localhost:27017";
  var lastMonth, month;
  var protobuf = require('protocol-buffers');
  MongoClient.connect(url, function(err, db) {
    var dbo = db.db("Crimes");
    var stats = [];
    //--------------------------------------------------------------get liste de mois  
    dbo.collection("crimes").distinct("DATE")
      .then(function(dates) {
        var listeCategories = ['Introduction', 'Méfait', 'Vols qualifiés',
          'Vol de véhicule à moteur', 'Vol dans / sur véhicule à moteur'
        ];
        dates.forEach(function(data) {
          month = data.substring(0, 8);
          if(month != lastMonth && month != 'DATE') {
            var leMois = data.substring(0, 7);
            var stat = [];
            //------------------------------------------------------get stats/categorie 			  
            listeCategories.forEach(function(categorie) {
              dbo.collection("crimes").find({
                  CATEGORIE: categorie,
                  DATE: new RegExp(month)
                }).count()
                .then(function(num) {stat.push({ type: categorie, nombre: num });
                });
            });
            stats.push({ mois: leMois, actes: stat });
          }
          lastMonth = month;
        });
        //-----------------------------------------------------------return buffer
        setTimeout(function(err) {
          if(err) {
            console.log("\n--> erreur lors du chargement de la page '/stats/buffer'");
            res.status(500);
            res.render('error');
          }
          var buf = Buffer.from(JSON.stringify(stats));
          res.send(buf);
        }, 3000);
      });
  });
});

//-------------------------------------------------------------------ROUTE '/STATS/HTML'
//-------------------------------------------------------------------------------------
//	-------> B1
//	La route «/stats/html» présente les données de A5 dans un document HTML. 
//  Au chargement de la page HTML, une requête Ajax invoque le service 
//  et affiche les statistiques reçues dans un tableau.

router.get('/stats/html', function(req, res, next) {
  setTimeout(function(err) {
    if(err) {
      console.log("\n--> erreur lors du chargement de la page '/stats/html'");
      res.status(500);
      res.render('error');
    }
    res.render('stats');
  }, 3000);
});

//-------------------------------------------------------------ROUTE '/STATS/HTML_CHART'
//-------------------------------------------------------------------------------------
//	-------> B2
//	La route «/stats/html_chart» ajoute un bar chart à la page 
//  développée pour B1, illustrant l’évolution des différents 
//  types d’actes criminels concernant les 4 derniers mois aux 
//  registres.

router.get('/stats/html_chart', function(req, res, next) {
  setTimeout(function(err) {

    res.render('chart');
  }, 3000);
});

//-------------------------------------------------------------ROUTE '/STATS/HTML_ESSAI'
//-------------------------------------------------------------------------------------
//	-------> B3
//	La route «/stats/html_essai» est en dehors du devis. 
//  Elle affiche les statistiques reçues pour B1 de façon à voir 
//  rapidement les écarts des actes pour tous les mois aux registres.

router.get('/stats/html_essai', function(req, res, next) {
  setTimeout(function(err) {
    if(err) {
      console.log("\n--> erreur lors du chargement de la page '/stats/html_essai'");
      res.status(500);
      res.render('error');
    }
    res.render('index');
  }, 3000);
});

//---------------------------------------------------------------------ROUTE '/ACTES3KM'
//-------------------------------------------------------------------------------------
//	-------> C1
//  La route «/actes3km» retourne la liste des actes criminels ayant
//  eu lieu dans les 3 derniers mois situés à moins de 3 km d’un point 
//  donné en paramètre. La distance doit être calculée selon la formule 
//  de haversine.

router.get('/actes3km', function(req, res, next) {
  var longitude = req.query.longitude;
  var latitude = req.query.latitude;
  var haversine = require('haversine');
  var MongoClient = require("mongodb").MongoClient;
  var url = "mongodb://localhost:27017";
  MongoClient.connect(url, function(err, db) {
    var dbo = db.db("Crimes");
    var liste = [];
    var crimes = dbo.collection("crimes").find();
    crimes.forEach(function(crime) {
      if(crime.DATE.substring(0, 7) == '2018-04' || crime.DATE.substring(0, 7) == '2018-03' ||
        crime.DATE.substring(0, 7) == '2018-02') {
        var start = { latitude: latitude, longitude: longitude };
        var end = { latitude: crime.LAT, longitude: crime.LONG };
        if(haversine(start, end, { unit: 'km' }) <= 3) {
          liste.push({
            CATEGORIE: crime.CATEGORIE,
            DATE: crime.DATE,
            QUART: crime.QUART,
            PDQ: parseInt(crime.PDQ),
            X: parseFloat(crime.X),
            Y: parseFloat(crime.Y),
            LAT: parseFloat(crime.LAT),
            LONG: parseFloat(crime.LONG)
          });
        }
      }
    }).then(function() {
      if(liste.length == 0) {
        res.status(404);
        res.render('error404');
      }
      res.json(liste);
      if(err) {
        res.status(500);
        res.render('error');
        console.log("\n--> erreur lors du chargement de la page '/lieux'");
      }
    });
  });
});

//--------------------------------------------------------------------------------D1.JS
//-------------------------------------------------------------------------------------
//	-------> D1
//	D1 extrait le code d’importation de données du module D1.js 
//	et lance un cron job afin d’importer automatiquement les données 
//	concernant les lieux culturels municipaux chaque jour à 13h31.

cronJob2 = new CronJob('00 31 13 * * *', function(err) {
    if(err) console.log("Une erreur est surevenue lors du CronJob pour " +
      "le fichier lieuxculturels.csv");
    var another2 = require('D1.js');
  }, function(err) {
    if(err) {
      res.status(500);
      res.render('error');
      console.log("\n--> erreur lors du CronJob(lieuxculturels.csv)\n");
    }
  },
  true
);

//------------------------------------------------------------------------ROUTE '/LIEUX'
//-------------------------------------------------------------------------------------
//	-------> D2
//  La route «/lieux» retourne la liste des lieux culturels à Montréal. 
//  Pour chaque lieu, on retourne également le nombre d’actes criminels 
//  ayant eu lieu à moins de 1 km du lieu en 2018. La  distance est calculée 
//  selon la formule de Haversine

router.get('/lieux', function(req, res, next) {
  var MongoClient = require("mongodb").MongoClient;
  var url = "mongodb://localhost:27017";
  MongoClient.connect(url, function(err, db) {
    var dbo = db.db("Crimes");
    var lieux = dbo.collection("lieux").find();
    lieux.toArray(function(err, liste) {
      if(err) {
        res.status(500);
        res.render('error');
        console.log("\n--> erreur lors du chargement de la page '/lieux'");
      }
      res.json(liste);
    });
  });
});

//-----------------------------------------------------------------------------ROUTE '/' 
//-------------------------------------------------------------------------------------
//	-------> ACCUEIL
//  Offre des boutons permettant à l'usager de naviguer facilement 
//  à travers les différentes routes offertes. 

router.get('/', function(req, res, next) {
  res.render('doc');
});

//------------------------------------------------------------------------------HEROKU'
//-------------------------------------------------------------------------------------
//	-------> F3
//  Le système est entièrement déployé sur la plateforme infonuagique Heroku
//  https://fathomless-dawn-43279.herokuapp.com/

//-------------------------------------------------------------------ERROR HANDLER 404
//-------------------------------------------------------------------------------------
//	-------> error 404
router.use(function(req, res, next) {
  res.status(404);
  res.render('error404');
});

//-------------------------------------------------------------------ERROR HANDLER 500
//-------------------------------------------------------------------------------------
//	-------> error 500
router.use(function(req, res, next) {
  res.status(500);
  res.render('error');
});

module.exports = router;