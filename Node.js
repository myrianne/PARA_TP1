//--------------------------------------------------------------------------------NODE.JS
//-------------------------------------------------------------------------------------
//	-------> A1
//	le script Node.js permet d’extraire les données de la ville de Montréal sur les 
//	actes criminels sur le territoire de la ville et de construire une base 
//	de données MongoDB(Crimes) avec ces données.

const get = require('simple-get')
var http = require('http');
var fs = require('fs');
var csv = require('csv');
var obj = csv();
var iconv = require("iconv-lite");

module.exports = {

  createDB: function () {
//--------------------------------------------------------------GET CSV AND COPY TO FILE
    var file = fs.createWriteStream("interventionscitoyendo.csv");
    var request = http.get("http://donnees.ville.montreal.qc.ca/dataset/5829b5b0-ea6f-476f-be94-bc2b8797769a/resource/c6f482bf-bf0f-4960-8b2f-9982c211addd/download/interventionscitoyendo.csv", function (response) {
      response.pipe(file);
    });
    var input = fs.readFileSync("interventionscitoyendo.csv", {
      encoding: "binary"
    });
    var output = iconv.decode(input, "ISO-8859-1");

//--------------------------------------------------------------CREATE DATABASE
    file.on('pipe', function () { console.log(" \n...data downloading... "); });
    request.on('close', function () {
      var MongoClient = require("mongodb").MongoClient;
      var url = "mongodb://localhost:27017";

      MongoClient.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("Crimes");
        var listCrimes = [];

        obj.from.path('interventionscitoyendo.csv', {encoding: "binary"}).to.array(function (data) {
          for (var index = 0; index < data.length; index++) {
            var crime = {
              CATEGORIE: data[index][0],
              DATE: data[index][1],
              QUART: data[index][2],
              PDQ: parseInt(data[index][3]),
              X: parseFloat(data[index][4]),
              Y: parseFloat(data[index][5]),
              LAT: parseFloat(data[index][6]),
              LONG: parseFloat(data[index][7])
            };
            console.log(crime);
            listCrimes.push(crime);
          }
          dbo.collection("crimes").drop();
          dbo.collection("crimes").insertMany(listCrimes, function (err, res) {
            if (err) throw err;
            console.log("\n  DATA INSERTED");
          });
          if (data.status == 200) { db.close(); }
        });
      });
    });
  }
};

//--------------------------------------------------------------run function(private)
var another = require('./Node.js');
another.createDB();
